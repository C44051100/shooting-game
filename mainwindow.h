#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QTimer>
#include <QMovie>
#include <time.h>
#include "bullet.h"
#include "enemy.h"
#include "boss.h"
#include "mob.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QGraphicsPixmapItem *player;

    void collide();
public slots:
    void keyPressEvent(QKeyEvent* event);
    void mousePressEvent(QMouseEvent *event);
private slots:
    void choose_background();
    void choose_character();
    void set_background_volcano();
    void set_background_tos();
    void next_page();
    void previous_page();
    void on_timeout();
    void gen_enemy_shoot();
    void change_character_next();
    void change_character_previous();
    void menu();
    void set_finish();
    void time_pause();
    void take_a_rest();
    void enemy_move_boss();
    void skill_time_pass();
signals:

private:
    int v_x;
    int v_y;
    int count;
    int specialskill;
    int specialskill_time;
    int pause_time;
    float count_time;
    int count_enemy_shoot;
    int count_rest;
    int bg_num;
    int character_num;
    int hp;
    int hp_now;
    int stage;
    int max_stage;
    int skill_time_count;
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QTimer timer;
    QTimer mytimer;
    QTimer timer_time;
    QTimer enemy_time;
    QTimer enemy_move_time;
    QTimer rest_time;
    QTimer skill_time;
    QImage *specialskill_img;
    QSize window_size;
    boss *b;
    mob *m;
};

#endif // MAINWINDOW_H

#ifndef BOSS_H
#define BOSS_H
#include <QTimer>
#include "enemy.h"

class boss : public enemy
{
public:
    boss();
signals:

public slots:
    virtual void enemy_move(int);
    virtual void attack();
    virtual void attacked();
private:
    QTimer bos_bullet_move;
};

#endif // BOSS_H

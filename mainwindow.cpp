#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    centralWidget()->setMouseTracking(true);
    setMouseTracking(true);
    ui->back->setIcon(QIcon(":/image/resourcefile/Redo_icon-icons.png"));
    ui->back->setIconSize(QSize(90,90));
    ui->back->setFlat(true);
    ui->pause->setFlat(true);
    ui->pause->setIcon(QIcon(":/image/resourcefile/Pause-icon.png"));
    ui->pause->setIconSize(QSize(85,85));
    ui->next->setFlat(true);
    ui->next->setIcon(QIcon(":/image/resourcefile/next"));
    ui->next->setIconSize(QSize(100,100));
    ui->next->hide();
    ui->previous->setFlat(true);
    ui->previous->setIcon(QIcon(":/image/resourcefile/download.png"));
    ui->previous->setIconSize(QSize(100,100));
    ui->previous->hide();
    ui->OK->hide();
    ui->character->hide();
    ui->frame_2->hide();
    ui->change_bg->setFlat(true);
    ui->character->setFlat(true);
    ui->option->setFlat(true);
    QPalette pal;
    pal.setColor(QPalette::ButtonText,QColor(255,128,0));
    ui->change_bg->setPalette(pal);
    ui->character->setPalette(pal);
    ui->option->setPalette(pal);
    ui->graphicsView->hide();
    ui->bground->setPixmap(QPixmap(":/image/resourcefile/KajeaJB.jpg"));
    bg_num=0;
    QImage *frame_img=new QImage(":/image/resourcefile/45561316.png");
    ui->frame->setPixmap(QPixmap::fromImage(*frame_img).scaled(190,140));
    ui->frame_2->setPixmap(QPixmap::fromImage(*frame_img).scaled(190,140));
    ui->frame_3->setPixmap(QPixmap::fromImage(*frame_img).scaled(190,140));
    ui->character->move(370,280);
    ui->change_bg->move(370,370);
    ui->option->move(370,460);
    ui->frame->move(340,350);
    ui->frame_2->move(340,260);
    ui->frame_3->move(340,440);
    ui->jungle->hide();
    ui->space->hide();
    ui->volcano->hide();
    ui->volcano->setIcon(QIcon(":/image/resourcefile/hearthstone_heroes_of_warcraft_for_button.jpg"));
    ui->volcano->setIconSize(QSize(260,650));
    ui->tos->hide();
    ui->tos->setIcon(QIcon(":/image/resourcefile/123318p1yh7cgychc1r1ne.png"));
    ui->tos->setIconSize(QSize(800,700));
    ui->character_label->hide();
    ui->check_bgm->hide();
    ui->check_se->hide();
    ui->check_bgm->move(360,200);
    ui->check_se->move(360,340);
    ui->set_bgm->hide();
    ui->set_bgm->move(320,270);
    ui->set_se->hide();
    ui->set_se->move(320,410);
    QPalette pal2;
    pal2.setColor(QPalette::Base,QColor(0,255,255));
    ui->spinBox_bgm->hide();
    ui->spinBox_bgm->move(550,265);
    ui->spinBox_bgm->setRange(0,100);
    ui->spinBox_bgm->setPalette(pal2);
    ui->spinBox_se->hide();
    ui->spinBox_se->move(550,405);
    ui->spinBox_se->setRange(0,100);
    ui->spinBox_se->setPalette(pal2);
    specialskill_img=new QImage;
    timer.setInterval(20);
    mytimer.setInterval(20);
    timer.start();
    mytimer.start();
    timer_time.setInterval(100);
    enemy_time.setInterval(3000);
    enemy_move_time.setInterval(1000);
    rest_time.setInterval(3000);
    skill_time.setInterval(5000);
    ui->enemy->move(830,225);
    ui->enemy->setPixmap(QPixmap(":/image/resourcefile/enemy.png").scaled(100,50));
    ui->enemy->hide();
    ui->player->move(830,355);
    ui->player->setPixmap(QPixmap(":/image/resourcefile/player.png").scaled(100,50));
    ui->player->hide();
    ui->score->move(830,480);
    ui->score->setPixmap(QPixmap(":/image/resourcefile/score.png").scaled(100,50));
    ui->score->hide();
    ui->skill_charge->move(830,630);
    ui->skill_charge->setPixmap(QPixmap(":/image/resourcefile/skill_charge.png").scaled(205,70));
    ui->skill_charge->hide();
    ui->time->move(830,90);
    ui->time->setPixmap(QPixmap(":/image/resourcefile/time.png").scaled(100,50));
    ui->time->hide();
    ui->lcdNumber->move(840,150);
    ui->lcdNumber_score->move(840,550);
    ui->lcdNumber->hide();
    ui->lcdNumber_score->hide();
    ui->save->move(360,500);
    ui->save->hide();
    ui->label_2->hide();
    ui->label_weapon->hide();
    ui->label_weapon_frame->hide();
    ui->label_hp_1->resize(QSize(40,50));
    ui->label_hp_1->setPixmap(QPixmap(":/image/resourcefile/9dde13d7.png").scaled(40,50));
    ui->label_hp_1->move(820,420);
    ui->label_hp_1->hide();
    ui->label_hp_2->resize(QSize(40,50));
    ui->label_hp_2->setPixmap(QPixmap(":/image/resourcefile/9dde13d7.png").scaled(40,50));
    ui->label_hp_2->move(850,420);
    ui->label_hp_2->hide();
    ui->label_hp_3->resize(QSize(40,50));
    ui->label_hp_3->setPixmap(QPixmap(":/image/resourcefile/9dde13d7.png").scaled(40,50));
    ui->label_hp_3->move(880,420);
    ui->label_hp_3->hide();
    ui->label_hp_4->resize(QSize(40,50));
    ui->label_hp_4->setPixmap(QPixmap(":/image/resourcefile/9dde13d7.png").scaled(40,50));
    ui->label_hp_4->move(910,420);
    ui->label_hp_4->hide();
    ui->label_hp_5->resize(QSize(40,50));
    ui->label_hp_5->setPixmap(QPixmap(":/image/resourcefile/9dde13d7.png").scaled(40,50));
    ui->label_hp_5->move(940,420);
    ui->label_hp_5->hide();
    ui->enemy_hp->move(830,300);
    ui->enemy_hp->resize(QSize(350,40));
    ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH10.png").scaled(350,40));
    ui->enemy_hp->hide();
    ui->win_or_lose->hide();
    ui->win_or_lose_char->hide();
    connect(ui->OK,SIGNAL(clicked(bool)),this,SLOT(next_page()));
    connect(ui->back,SIGNAL(clicked(bool)),this,SLOT(previous_page()));
    connect(ui->change_bg,SIGNAL(clicked(bool)),this,SLOT(choose_background()));
    connect(ui->character,SIGNAL(clicked(bool)),this,SLOT(choose_character()));
    connect(&timer_time,SIGNAL(timeout()),this,SLOT(on_timeout()));
    connect(&enemy_time,SIGNAL(timeout()),this,SLOT(gen_enemy_shoot()));
    connect(&rest_time,SIGNAL(timeout()),this,SLOT(take_a_rest()));
    connect(ui->volcano,SIGNAL(clicked(bool)),this,SLOT(set_background_volcano()));
    connect(ui->tos,SIGNAL(clicked(bool)),this,SLOT(set_background_tos()));
    connect(ui->next,SIGNAL(clicked(bool)),this,SLOT(change_character_next()));
    connect(ui->previous,SIGNAL(clicked(bool)),this,SLOT(change_character_previous()));
    connect(ui->option,SIGNAL(clicked(bool)),this,SLOT(menu()));
    connect(ui->save,SIGNAL(clicked(bool)),this,SLOT(set_finish()));
    connect(ui->spinBox_bgm,SIGNAL(valueChanged(int)),ui->set_bgm,SLOT(setValue(int)));
    connect(ui->set_bgm,SIGNAL(valueChanged(int)),ui->spinBox_bgm,SLOT(setValue(int)));
    connect(ui->spinBox_se,SIGNAL(valueChanged(int)),ui->set_se,SLOT(setValue(int)));
    connect(ui->set_se,SIGNAL(valueChanged(int)),ui->spinBox_se,SLOT(setValue(int)));
    connect(ui->pause,SIGNAL(clicked(bool)),this,SLOT(time_pause()));
    connect(&skill_time,SIGNAL(timeout()),this,SLOT(skill_time_pass()));
    count=0;
    v_x=0;
    v_y=0;
    stage=1;
    hp=5;
    hp_now=5;
    pause_time=0;
    count_enemy_shoot=0;
    count_rest=0;
    skill_time_count=0;
    window_size=QSize(850,600);
    ui->character_label->move(220,50);
    b=new boss();
    m=new mob();
    qsrand(time(NULL));
}
void MainWindow::choose_background(){
    ui->change_bg->hide();
    ui->character->hide();
    ui->option->hide();
    ui->frame->hide();
    ui->frame_2->hide();
    ui->frame_3->hide();
    ui->volcano->move(200,300);
    ui->tos->move(500,300);
    if(bg_num==3){
        ui->volcano->move(50,350);
        ui->tos->move(300,350);
    }
    ui->volcano->show();
    ui->tos->show();
}
void MainWindow::set_background_volcano(){
    if(this->size()!=QSize(850,600))this->resize(850,600);
    this->move(500,150);
    window_size=QSize(850,600);
    ui->bground->setPixmap(QPixmap(":/image/resourcefile/hearthstone_heroes_of_warcraft_.jpg").scaled(850,550));
    ui->bground->move(0,-100);
    ui->jungle->hide();
    ui->space->hide();
    ui->volcano->hide();
    ui->tos->hide();
    ui->change_bg->show();
    ui->character->show();
    ui->option->show();
    ui->frame->show();
    ui->frame_2->show();
    ui->frame_3->show();
    ui->previous->move(0,220);
    ui->next->move(730,220);
    ui->OK->move(380,500);
    ui->jungle->move(180,150);
    ui->space->move(450,150);
    ui->volcano->move(180,360);
    ui->tos->move(450,360);
    ui->character_label->move(220,50);
    ui->check_bgm->move(360,200);
    ui->check_se->move(360,340);
    ui->set_bgm->move(320,270);
    ui->set_se->move(320,410);
    ui->spinBox_bgm->move(550,265);
    ui->spinBox_se->move(550,405);
    ui->save->move(360,500);
    QPalette pal;
    pal.setColor(QPalette::ButtonText,QColor(0,255,255));
    ui->change_bg->setPalette(pal);
    ui->character->setPalette(pal);
    ui->option->setPalette(pal);
    bg_num=2;
}
void MainWindow::set_background_tos(){
    if(this->size()!=QSize(530,800))this->resize(QSize(530,800));
    this->move(650,80);
    window_size=QSize(530,800);
    ui->bground->setPixmap(QPixmap(":/image/resourcefile/123318p1yh7cgychc1r1ne.png"));
    ui->bground->move(0,0);
    ui->jungle->hide();
    ui->space->hide();
    ui->volcano->hide();
    ui->tos->hide();
    ui->change_bg->show();
    ui->character->show();
    ui->option->show();
    ui->frame->show();
    ui->frame_2->show();
    ui->frame_3->show();
    ui->previous->move(0,620);
    ui->next->move(400,620);
    ui->OK->move(220,720);
    ui->jungle->move(30,180);
    ui->space->move(300,180);
    ui->volcano->move(30,390);
    ui->tos->move(300,390);
    ui->character_label->move(75,140);
    ui->check_bgm->move(230,200);
    ui->check_se->move(230,340);
    ui->set_bgm->move(200,270);
    ui->set_se->move(200,410);
    ui->spinBox_bgm->move(420,265);
    ui->spinBox_se->move(420,405);
    ui->save->move(220,720);
    QPalette pal;
    pal.setColor(QPalette::ButtonText,QColor(200,80,255));
    ui->change_bg->setPalette(pal);
    ui->character->setPalette(pal);
    ui->option->setPalette(pal);
    bg_num=3;
}
void MainWindow::choose_character(){
    ui->change_bg->hide();
    ui->character->hide();
    ui->option->hide();
    ui->frame->hide();
    ui->frame_2->hide();
    ui->frame_3->hide();
    ui->next->show();
    ui->previous->show();
    ui->OK->show();
    character_num=0;
    if(bg_num==2)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/warrior.png").scaled(370,400));;
    if(bg_num==3)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/998546.png").scaled(370,400));
    ui->character_label->show();
}
void MainWindow::change_character_next(){
    character_num++;
    if(bg_num==2){
        if(character_num==9)character_num=0;
        if(character_num==0)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/warrior.png").scaled(370,400));
        if(character_num==1)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/shaman.png").scaled(370,400));
        if(character_num==2)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/rogue.png").scaled(370,400));
        if(character_num==3)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/paladin.png").scaled(370,400));
        if(character_num==4)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/hunter.png").scaled(370,400));
        if(character_num==5)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/druid.png").scaled(370,430));
        if(character_num==6)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/warlock.png").scaled(370,400));
        if(character_num==7)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/mage.png").scaled(370,400));
        if(character_num==8)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/priest.png").scaled(370,400));

    }
    if(bg_num==3){
        if(character_num==5)character_num=0;
        if(character_num==0)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/998546.png").scaled(370,400));
        if(character_num==1)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/41654616.png").scaled(370,400));
        if(character_num==2)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/873164.png").scaled(370,400));
        if(character_num==3)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/845633.png").scaled(370,400));
        if(character_num==4)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/546566.png").scaled(370,400));
    }
}
void MainWindow::change_character_previous(){
    character_num--;
    if(bg_num==2){
        if(character_num==-1)character_num=8;
        if(character_num==0)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/warrior.png").scaled(370,400));
        if(character_num==1)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/shaman.png").scaled(370,400));
        if(character_num==2)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/rogue.png").scaled(370,400));
        if(character_num==3)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/paladin.png").scaled(370,400));
        if(character_num==4)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/hunter.png").scaled(370,400));
        if(character_num==5)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/druid.png").scaled(370,430));
        if(character_num==6)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/warlock.png").scaled(370,400));
        if(character_num==7)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/mage.png").scaled(370,400));
        if(character_num==8)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/priest.png").scaled(370,400));
    }
    if(bg_num==3){
        if(character_num==-1)character_num=4;
        if(character_num==0)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/998546.png").scaled(390,450));
        if(character_num==1)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/41654616.png").scaled(370,400));
        if(character_num==2)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/873164.png").scaled(370,450));
        if(character_num==3)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/845633.png").scaled(370,400));
        if(character_num==4)ui->character_label->setPixmap(QPixmap(":/image/resourcefile/546566.png").scaled(390,410));
    }
}
void MainWindow::menu(){
    ui->frame->hide();
    ui->character->hide();
    ui->frame_2->hide();
    ui->change_bg->hide();
    ui->frame_3->hide();
    ui->option->hide();
    ui->check_bgm->show();
    ui->check_se->show();
    ui->set_bgm->show();
    ui->set_se->show();
    ui->spinBox_bgm->show();
    ui->spinBox_se->show();
    ui->save->show();
}
void MainWindow::set_finish(){
    ui->check_bgm->hide();
    ui->check_se->hide();
    ui->set_bgm->hide();
    ui->set_se->hide();
    ui->spinBox_bgm->hide();
    ui->spinBox_se->hide();
    ui->save->hide();
    ui->frame->show();
    ui->change_bg->show();
    if((bg_num==2)||(bg_num==3)){
        ui->character->show();
        ui->frame_2->show();
    }
    ui->option->show();
    ui->frame_3->show();
}
void MainWindow::next_page(){
    ui->OK->hide();
    ui->previous->hide();
    ui->next->hide();
    ui->bground->hide();
    ui->label_2->move(0,-370);
    ui->label_2->resize(1500,1500);
    ui->label_2->setPixmap(QPixmap(":/image/resourcefile/gatag-00002888.jpg").scaled(1300,1000));
    ui->label_2->show();
    ui->label_hp_1->show();
    ui->label_hp_2->show();
    ui->label_hp_3->show();
    ui->label_hp_4->show();
    ui->label_hp_5->show();
    this->resize(1200,850);
    this->move(450,40);
    ui->time->show();
    ui->player->show();
    ui->enemy->show();
    ui->score->show();
    ui->lcdNumber->show();
    ui->lcdNumber_score->show();
    ui->skill_charge->show();
    ui->enemy_hp->show();
    ui->enemy_pix->show();
    ui->graphicsView->show();
    ui->graphicsView->resize(QSize(800,800));
    scene=new QGraphicsScene(0,0,780,780);
    ui->graphicsView->setBackgroundBrush(QBrush(Qt::black,Qt::SolidPattern));
    ui->graphicsView->setScene(scene);
    if(bg_num==0){

    }
    if(bg_num==1){

    }
    if(bg_num==2){
        b->setPixmap(QPixmap(":/image/resourcefile/warrior.png").scaled(100,115));
        b->hp=90;
        max_stage=9;
        ui->label_weapon->show();
        ui->label_weapon->resize(QSize(150,150));
        ui->label_weapon_frame->show();
        ui->label_weapon_frame->resize(QSize(200,280));
        ui->label_weapon_frame->setPixmap(QPixmap(":/image/resourcefile/73af7636.png").scaled(200,280));
        if(character_num==0){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/warrior.png").scaled(70,80));
            ui->label_weapon->move(1010,360);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/warrior_hero_power.png").scaled(150,150));
        }
        if(character_num==1){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/shaman.png").scaled(70,80));
            ui->label_weapon->move(1030,360);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/shaman_hero_power.png").scaled(110,110));
        }
        if(character_num==2){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/rogue.png").scaled(70,80));
            ui->label_weapon->move(1000,360);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/rogue_hero_power.png").scaled(150,150));
        }
        if(character_num==3){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/paladin.png").scaled(70,80));
            ui->label_weapon->move(990,360);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/paladin_hero_power.png").scaled(180,180));
        }
        if(character_num==4){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/hunter.png").scaled(70,80));
            ui->label_weapon->move(970,380);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/hunter_hero_power.png"));
        }
        if(character_num==5){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/druid.png").scaled(70,80));
            ui->label_weapon->move(1015,380);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/druid_hero_power.png").scaled(130,130));
        }
        if(character_num==6){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/warlock.png").scaled(70,80));
            ui->label_weapon->move(1040,390);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/warlock_hero_power.png").scaled(80,150));
        }
        if(character_num==7){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/mage.png").scaled(70,80));
            ui->label_weapon->move(1030,390);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/mage_hero_power.png").scaled(100,150));
        }
        if(character_num==8){
            player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/priest.png").scaled(70,80));
            ui->label_weapon->move(1020,360);
            ui->label_weapon->setPixmap(QPixmap(":/image/resourcefile/priest_hero_power.png").scaled(130,130));
        }
    }
    if(bg_num==3){
        m->setPixmap(QPixmap(":/image/resourcefile/tos_mob_1.png").scaled(50,50));
        m->hp=30;
        max_stage=11;
        if(character_num==0)player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/653155.png").scaled(70,70));
        if(character_num==1)player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/586455.png").scaled(70,70));
        if(character_num==2)player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/6845456.png").scaled(70,70));
        if(character_num==3)player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/84556.png").scaled(70,70));
        if(character_num==4)player=new QGraphicsPixmapItem(QPixmap(":/image/resourcefile/7986453.png").scaled(70,70));
    }
    scene->addItem(player);
    player->setPos(375,650);
    if(bg_num==2){
        b->setPos(375,50);
        scene->addItem(b);
        connect(&enemy_move_time,SIGNAL(timeout()),this,SLOT(enemy_move_boss()));
        connect(&timer,SIGNAL(timeout()),b,SLOT(attack()));
        connect(&timer,SIGNAL(timeout()),b,SLOT(attacked()));
    }
    if(bg_num==3){
        m->setPos(375,50);
        scene->addItem(m);
        connect(&enemy_move_time,SIGNAL(timeout()),m,SLOT(enemy_move()));
        connect(&timer,SIGNAL(timeout()),m,SLOT(attack()));
        connect(&timer,SIGNAL(timeout()),m,SLOT(attacked()));
    }
    count_time=-30;
    specialskill=970;
    timer_time.start();
    ui->label_gif->hide();
    ui->frame->hide();
    ui->change_bg->hide();
    ui->frame_2->hide();
    ui->character->hide();
    ui->frame_3->hide();
    ui->option->hide();
    ui->jungle->hide();
    ui->space->hide();
    ui->volcano->hide();
    ui->character_label->hide();
}
void MainWindow::previous_page(){
    if(window_size==QSize(530,800)){
        this->resize(QSize(530,800));
        this->move(650,80);
    }
    if(window_size==QSize(850,600)){
        this->resize(850,600);
        this->move(500,150);
    }
    ui->OK->hide();
    ui->previous->hide();
    ui->next->hide();
    ui->bground->show();
    ui->graphicsView->hide();
    ui->frame->show();
    ui->change_bg->show();
    ui->frame_2->show();
    ui->character->show();
    ui->frame_3->show();
    ui->option->show();
    ui->label_2->hide();
    ui->time->hide();
    ui->enemy->hide();
    ui->player->hide();
    ui->score->hide();
    ui->skill_charge->hide();
    ui->lcdNumber->hide();
    ui->lcdNumber_score->hide();
    ui->label_weapon->hide();
    ui->label_weapon_frame->hide();
    ui->label_hp_1->hide();
    ui->label_hp_2->hide();
    ui->label_hp_3->hide();
    ui->label_hp_4->hide();
    ui->label_hp_5->hide();
    ui->pause->setIcon(QIcon(":/image/resourcefile/Pause-icon.png"));
    ui->enemy_hp->hide();
    ui->enemy_pix->hide();
    pause_time=0;
    stage=1;
    hp=5;
    hp_now=5;
    enemy_move_time.stop();
}
void MainWindow::keyPressEvent(QKeyEvent *event){
    if(pause_time%2==0){
        if(this->size()==QSize(1200,850)){
            if((event->key()==Qt::Key_A)&&(player->pos().x()>0)){
                player->setPos(player->pos().x()-12,player->pos().y());
                v_x=-2;
                v_y=0;
            }
            if((event->key()==Qt::Key_D)&&(player->pos().x()<750)){
                player->setPos(player->pos().x()+12,player->pos().y());
                v_x=2;
                v_y=0;
            }
            if((event->key()==Qt::Key_W)&&(player->pos().y()>0)){
                player->setPos(player->pos().x(),player->pos().y()-12);
                v_x=0;
                v_y=2;
            }
            if((event->key()==Qt::Key_S)&&(player->pos().y()<750)){
                player->setPos(player->pos().x(),player->pos().y()+12);
                v_x=0;
                v_y=-2;
            }
        }
    }
}
void MainWindow::mousePressEvent(QMouseEvent *event){
    if(pause_time%2==0){
        if((this->size()==QSize(1200,850))&&(count_time>0)){
            if(event->button()==Qt::LeftButton){

                if(bg_num==2){
                    if(character_num==0){
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/warrior_hero_power.png");
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(90,90));
                        scene->addItem(b2);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-90);

                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        b2->list=scene->collidingItems(b2);
                    }
                    if(character_num==1){
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/shaman_hero_power.png");
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(50,70));
                        scene->addItem(b2);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-90);

                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        b2->list=scene->collidingItems(b2);
                    }
                    if(character_num==2){
                        bullet *b1=new bullet(v_x,v_y);
                        b1->ph=5;
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        bullet *b3=new bullet(v_x,v_y);
                        b3->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/rogue_hero_power.png");
                        b1->setPixmap(QPixmap::fromImage(*bullet).scaled(55,55));
                        scene->addItem(b1);
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(55,55));
                        scene->addItem(b2);
                        b3->setPixmap(QPixmap::fromImage(*bullet).scaled(55,55));
                        scene->addItem(b3);
                        b1->setPos(player->pos().x()-b1->pixmap().width()*0.7+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-55);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-60);
                        b3->setPos(player->pos().x()+b1->pixmap().width()*0.7+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-55);

                        connect(&mytimer,SIGNAL(timeout()),b1,SLOT(fly1()));
                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        connect(&mytimer,SIGNAL(timeout()),b3,SLOT(fly3()));
                        b1->list=scene->collidingItems(b1);
                    }
                    if(character_num==3){
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/paladin_hero_power.png");
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(90,90));
                        scene->addItem(b2);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-90);
                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        b2->list=scene->collidingItems(b2);
                    }
                    if(character_num==4){
                        bullet *b1=new bullet(v_x,v_y);
                        b1->ph=5;
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        bullet *b3=new bullet(v_x,v_y);
                        b3->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/hunter_hero_power.png");
                        b1->setPixmap(QPixmap::fromImage(*bullet).scaled(55,55));
                        scene->addItem(b1);
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(55,55));
                        scene->addItem(b2);
                        b3->setPixmap(QPixmap::fromImage(*bullet).scaled(55,55));
                        scene->addItem(b3);
                        b1->setPos(player->pos().x()-b1->pixmap().width()*0.7+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-50);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-55);
                        b3->setPos(player->pos().x()+b1->pixmap().width()*0.7+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-50);
                        connect(&mytimer,SIGNAL(timeout()),b1,SLOT(fly1()));
                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        connect(&mytimer,SIGNAL(timeout()),b3,SLOT(fly3()));
                        b1->list=scene->collidingItems(b1);
                    }
                    if(character_num==5){
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/druid_hero_power.png");
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(70,70));
                        scene->addItem(b2);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-90);
                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        b2->list=scene->collidingItems(b2);
                    }
                    if(character_num==6){
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/warlock_hero_power.png");
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(50,80));
                        scene->addItem(b2);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-90);
                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        b2->list=scene->collidingItems(b2);
                    }
                    if(character_num==7){
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/mage_hero_power.png");
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(30,60));
                        scene->addItem(b2);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-60);
                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        b2->list=scene->collidingItems(b2);
                    }
                    if(character_num==8){
                        bullet *b2=new bullet(v_x,v_y);
                        b2->ph=5;
                        QImage *bullet=new QImage;
                        bullet->load(":/image/resourcefile/priest_hero_power.png");
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(40,60));
                        scene->addItem(b2);
                        b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-60);

                        connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                        b2->list=scene->collidingItems(b2);
                    }

                }
                if(bg_num==3){
                    bullet *b1=new bullet(v_x,v_y);
                    b1->ph=5;
                    bullet *b2=new bullet(v_x,v_y);
                    b2->ph=5;
                    bullet *b3=new bullet(v_x,v_y);
                    b3->ph=5;
                    QImage *bullet=new QImage;
                    if(character_num==0)bullet->load(":/image/resourcefile/ball-149921_blue.png");
                    if(character_num==1)bullet->load(":/image/resourcefile/ball-149921_red.png");
                    if((character_num==2)||(character_num==3))bullet->load(":/image/resourcefile/ball-149921_orange.png");
                    if(character_num==4)bullet->load(":/image/resourcefile/ball-149921_green.png");
                    if(count%5!=4){
                        b1->setPixmap(QPixmap::fromImage(*bullet).scaled(10,10));
                        scene->addItem(b1);
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(10,10));
                        scene->addItem(b2);
                        b3->setPixmap(QPixmap::fromImage(*bullet).scaled(10,10));
                        scene->addItem(b3);
                    }
                    if(count%5==4){
                        b1->setPixmap(QPixmap::fromImage(*bullet).scaled(15,15));
                        scene->addItem(b1);
                        b2->setPixmap(QPixmap::fromImage(*bullet).scaled(15,15));
                        scene->addItem(b2);
                        b3->setPixmap(QPixmap::fromImage(*bullet).scaled(15,15));
                        scene->addItem(b3);
                    }
                    b1->setPos(player->pos().x()-b1->pixmap().width()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-15);
                    b2->setPos(player->pos().x()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-20);
                    b3->setPos(player->pos().x()+b1->pixmap().width()+(player->pixmap().width()-b2->pixmap().width())/2,player->pos().y()-15);
                    connect(&mytimer,SIGNAL(timeout()),b1,SLOT(fly1()));
                    connect(&mytimer,SIGNAL(timeout()),b2,SLOT(fly2()));
                    connect(&mytimer,SIGNAL(timeout()),b3,SLOT(fly3()));
                    b1->list=scene->collidingItems(b1);
                }
                v_x=0;
                v_y=0;
                count++;
            }
            if((event->button()==Qt::RightButton)&&(specialskill>=999)){
                specialskill-=1015;
                QMovie *specialskill_gif=new QMovie(":/image/resourcefile/10-0.gif");
                ui->label_gif->setMovie(specialskill_gif);
                specialskill_gif->start();
                ui->label_gif->show();
                hp=5;
                hp_now=5;
                ui->label_hp_5->show();
                ui->label_hp_4->show();
                ui->label_hp_3->show();
                ui->label_hp_2->show();
                ui->label_hp_1->show();
                timer.stop();
                enemy_move_time.stop();
                enemy_time.stop();
                skill_time.start();
            }
        }
    }

}
void MainWindow::on_timeout(){
    count_time++;
    if(hp!=hp_now){
        if(hp_now==4){
            ui->label_hp_5->hide();
        }
        if(hp_now==3){
            ui->label_hp_4->hide();
            ui->label_hp_5->hide();
        }
        if(hp_now==2){
            ui->label_hp_3->hide();
            ui->label_hp_4->hide();
            ui->label_hp_5->hide();
        }
        if(hp_now==1){
            ui->label_hp_2->hide();
            ui->label_hp_3->hide();
            ui->label_hp_4->hide();
            ui->label_hp_5->hide();
        }

        if(hp_now==0){
            ui->label_hp_1->hide();
            ui->label_hp_2->hide();
            ui->label_hp_3->hide();
            ui->label_hp_4->hide();
            ui->label_hp_5->hide();
            ui->win_or_lose->move(100,100);
            ui->win_or_lose->resize(QSize(700,700));
            ui->win_or_lose->setPixmap(QPixmap(":/image/resourcefile/hearthstone_lose.png").scaled(700,700));
            ui->win_or_lose->show();
            timer.stop();
            mytimer.stop();
            timer_time.stop();
            enemy_move_time.stop();
            enemy_time.stop();
            pause_time++;
        }
        hp=hp_now;
        player->setPos(375,650);
        player->show();
    }
    if(player->scene()->collidingItems(player).isEmpty()==false){
        player->hide();
        hp_now-=1;
    }
    if(bg_num==2){
        if(b->hp/9==10)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH10.png").scaled(350,40));
        if(b->hp/9==9)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH9.png").scaled(350,40));
        if(b->hp/9==8)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH8.png").scaled(350,40));
        if(b->hp/9==7)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH7.png").scaled(350,40));
        if(b->hp/9==6)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH6.png").scaled(350,40));
        if(b->hp/9==5)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH5.png").scaled(350,40));
        if(b->hp/9==4)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH4.png").scaled(350,40));
        if(b->hp/9==3)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH3.png").scaled(350,40));
        if(b->hp/9==2)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH2.png").scaled(350,40));
        if(b->hp/9==1)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH1.png").scaled(350,40));
        if(b->hp/9==0)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH0.png").scaled(350,50));
    }
    if(bg_num==3&&stage>3){
        if(b->hp/15==10)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH10.png").scaled(350,40));
        if(b->hp/15==9)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH9.png").scaled(350,40));
        if(b->hp/15==8)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH8.png").scaled(350,40));
        if(b->hp/15==7)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH7.png").scaled(350,40));
        if(b->hp/15==6)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH6.png").scaled(350,40));
        if(b->hp/15==5)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH5.png").scaled(350,40));
        if(b->hp/15==4)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH4.png").scaled(350,40));
        if(b->hp/15==3)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH3.png").scaled(350,40));
        if(b->hp/15==2)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH2.png").scaled(350,40));
        if(b->hp/15==1)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH1.png").scaled(350,40));
        if(b->hp/15==0)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH0.png").scaled(350,50));
    }
    if(bg_num==3&&stage<=3){
        if(m->hp/3==10)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH10.png").scaled(350,40));
        if(m->hp/3==9)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH9.png").scaled(350,40));
        if(m->hp/3==8)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH8.png").scaled(350,40));
        if(m->hp/3==7)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH7.png").scaled(350,40));
        if(m->hp/3==6)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH6.png").scaled(350,40));
        if(m->hp/3==5)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH5.png").scaled(350,40));
        if(m->hp/3==4)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH4.png").scaled(350,40));
        if(m->hp/3==3)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH3.png").scaled(350,40));
        if(m->hp/3==2)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH2.png").scaled(350,40));
        if(m->hp/3==1)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH1.png").scaled(350,40));
        if(m->hp/3==0)ui->enemy_hp->setPixmap(QPixmap(":/image/resourcefile/PH0.png").scaled(350,50));
    }

    if(count_time<=0)ui->lcdNumber->display(0);
    if(count_time==0){
        enemy_time.start();
        enemy_move_time.start();
    }
    if(count_time>0){
        ui->lcdNumber->display(count_time/10);
        ui->lcdNumber_score->display(count_time*10+stage*100);
    }
    if(specialskill==0){
        specialskill_img->load(":/image/resourcefile/0 0.png");
    }
    if(specialskill==24)ui->label_gif->hide();
    if(specialskill==100){
        specialskill_img->load(":/image/resourcefile/0-1 14.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/0-1.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==113)ui->label_gif->hide();
    if(specialskill==200){
        specialskill_img->load(":/image/resourcefile/1-2 21.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/1-2.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==216)ui->label_gif->hide();
    if(specialskill==300){
        specialskill_img->load(":/image/resourcefile/2-3 21.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/2-3.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==316)ui->label_gif->hide();
    if(specialskill==400){
        specialskill_img->load(":/image/resourcefile/3-4 23.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/3-4.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==416)ui->label_gif->hide();
    if(specialskill==500){
        specialskill_img->load(":/image/resourcefile/4-5 22.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/4-5.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==516)ui->label_gif->hide();
    if(specialskill==600){
        specialskill_img->load(":/image/resourcefile/5-6 23.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/5-6.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==616)ui->label_gif->hide();
    if(specialskill==700){
        specialskill_img->load(":/image/resourcefile/6-7 24.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/6-7.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==716)ui->label_gif->hide();
    if(specialskill==800){
        specialskill_img->load(":/image/resourcefile/7-8 22.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/7-8.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==816)ui->label_gif->hide();
    if(specialskill==900){
        specialskill_img->load(":/image/resourcefile/8-9 23.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/8-9.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==916)ui->label_gif->hide();
    if(specialskill==999){
        specialskill_img->load(":/image/resourcefile/9-10 24.png");
        QMovie *specialskill_gif=new QMovie(":/image/resourcefile/9-10.gif");
        ui->label_gif->setMovie(specialskill_gif);
        specialskill_gif->start();
        ui->label_gif->show();
    }
    if(specialskill==1015)ui->label_gif->hide();
    ui->label->setPixmap(QPixmap::fromImage(*specialskill_img));
    if(specialskill<1015)specialskill++;
}
void MainWindow::gen_enemy_shoot(){
    count_enemy_shoot++;
    if((bg_num==2)||(bg_num==3&&stage>3)){
        if(b->hp!=0){
            bullet *eb1=new bullet(0,0);
            eb1->ph=13;
            bullet *eb2=new bullet(0,0);
            eb2->ph=13;
            bullet *eb3=new bullet(0,0);
            eb3->ph=13;
            bullet *eb4=new bullet(0,0);
            eb4->ph=13;
            bullet *eb5=new bullet(0,0);
            eb5->ph=13;
            eb1->setPixmap(QPixmap(":/image/resourcefile/ball-149921_1280.png").scaled(20,20));
            eb2->setPixmap(QPixmap(":/image/resourcefile/ball-149921_1280.png").scaled(20,20));
            eb3->setPixmap(QPixmap(":/image/resourcefile/ball-149921_1280.png").scaled(20,20));
            eb4->setPixmap(QPixmap(":/image/resourcefile/ball-149921_1280.png").scaled(20,20));
            eb5->setPixmap(QPixmap(":/image/resourcefile/ball-149921_1280.png").scaled(20,20));
            scene->addItem(eb1);
            scene->addItem(eb2);
            scene->addItem(eb3);
            scene->addItem(eb4);
            scene->addItem(eb5);
            int a=qrand()%4;
            if(a==0){
                eb1->setPos(b->pos().x()-2*b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb2->setPos(b->pos().x()-b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb3->setPos(b->pos().x(),b->pos().y()+b->pixmap().height());
                eb4->setPos(b->pos().x()+b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb5->setPos(b->pos().x()+2*b->pixmap().width(),b->pos().y()+b->pixmap().height());
                connect(&timer,SIGNAL(timeout()),eb1,SLOT(bfly1()));
                connect(&timer,SIGNAL(timeout()),eb2,SLOT(bfly1()));
                connect(&timer,SIGNAL(timeout()),eb3,SLOT(bfly1()));
                connect(&timer,SIGNAL(timeout()),eb4,SLOT(bfly1()));
                connect(&timer,SIGNAL(timeout()),eb5,SLOT(bfly1()));
            }
            if(a==1){
                eb1->setPos(b->pos().x()-2*b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb2->setPos(b->pos().x()-b->pixmap().width(),b->pos().y()+b->pixmap().height()+20);
                eb3->setPos(b->pos().x(),b->pos().y()+b->pixmap().height()+80);
                eb4->setPos(b->pos().x()+b->pixmap().width(),b->pos().y()+b->pixmap().height()+40);
                eb5->setPos(b->pos().x()+2*b->pixmap().width(),b->pos().y()+b->pixmap().height()+60);
                connect(&timer,SIGNAL(timeout()),eb1,SLOT(bfly2()));
                connect(&timer,SIGNAL(timeout()),eb2,SLOT(bfly2()));
                connect(&timer,SIGNAL(timeout()),eb3,SLOT(bfly4()));
                connect(&timer,SIGNAL(timeout()),eb4,SLOT(bfly3()));
                connect(&timer,SIGNAL(timeout()),eb5,SLOT(bfly3()));
            }
            if(a==2){
                eb1->setPos(b->pos().x()-2*b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb2->setPos(b->pos().x()-b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb3->setPos(b->pos().x(),b->pos().y()+b->pixmap().height());
                eb4->setPos(b->pos().x()+b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb5->setPos(b->pos().x()+2*b->pixmap().width(),b->pos().y()+b->pixmap().height());
                connect(&timer,SIGNAL(timeout()),eb1,SLOT(bfly4()));
                connect(&timer,SIGNAL(timeout()),eb2,SLOT(bfly4()));
                connect(&timer,SIGNAL(timeout()),eb3,SLOT(bfly4()));
                connect(&timer,SIGNAL(timeout()),eb4,SLOT(bfly4()));
                connect(&timer,SIGNAL(timeout()),eb5,SLOT(bfly4()));
            }
            if(a==3){
                eb1->setPos(b->pos().x()-2*b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb2->setPos(b->pos().x()-b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb3->setPos(b->pos().x(),b->pos().y()+b->pixmap().height());
                eb4->setPos(b->pos().x()+b->pixmap().width(),b->pos().y()+b->pixmap().height());
                eb5->setPos(b->pos().x()+2*b->pixmap().width(),b->pos().y()+b->pixmap().height());
                connect(&timer,SIGNAL(timeout()),eb1,SLOT(bfly5()));
                connect(&timer,SIGNAL(timeout()),eb2,SLOT(bfly5()));
                connect(&timer,SIGNAL(timeout()),eb3,SLOT(bfly4()));
                connect(&timer,SIGNAL(timeout()),eb4,SLOT(bfly6()));
                connect(&timer,SIGNAL(timeout()),eb5,SLOT(bfly6()));
            }
        }
        if(b->hp<=0){
            stage++;
            rest_time.start();
            timer.stop();
            mytimer.stop();
            timer_time.stop();
            enemy_move_time.stop();
            enemy_time.stop();
            if(stage<=max_stage){
                if(bg_num==2){
                    if(stage==2)b->setPixmap(QPixmap(":/image/resourcefile/shaman.png").scaled(100,115));
                    if(stage==3)b->setPixmap(QPixmap(":/image/resourcefile/rogue.png").scaled(100,115));
                    if(stage==4)b->setPixmap(QPixmap(":/image/resourcefile/paladin.png").scaled(100,115));
                    if(stage==5)b->setPixmap(QPixmap(":/image/resourcefile/hunter.png").scaled(100,115));
                    if(stage==6)b->setPixmap(QPixmap(":/image/resourcefile/druid.png").scaled(100,115));
                    if(stage==7)b->setPixmap(QPixmap(":/image/resourcefile/warlock.png").scaled(100,115));
                    if(stage==8)b->setPixmap(QPixmap(":/image/resourcefile/mage.png").scaled(100,115));
                    if(stage==9)b->setPixmap(QPixmap(":/image/resourcefile/priest.png").scaled(100,115));
                    b->hp=90;
                }
                if(bg_num==3&&stage>3){
                    if(stage==5)b->setPixmap(QPixmap(":/image/resourcefile/79865465.png").scaled(100,100));
                    if(stage==6)b->setPixmap(QPixmap(":/image/resourcefile/88486453.png").scaled(100,100));
                    if(stage==7)b->setPixmap(QPixmap(":/image/resourcefile/6845334.png").scaled(100,100));
                    if(stage==8)b->setPixmap(QPixmap(":/image/resourcefile/683531.png").scaled(100,100));
                    if(stage==9)b->setPixmap(QPixmap(":/image/resourcefile/9865466.png").scaled(100,100));
                    if(stage==10)b->setPixmap(QPixmap(":/image/resourcefile/465321.png").scaled(100,100));
                    if(stage==11)b->setPixmap(QPixmap(":/image/resourcefile/9ths.png").scaled(100,100));
                    b->hp=150;
                }
            }
        }
        if(stage>max_stage){
            ui->win_or_lose->move(100,100);
            ui->win_or_lose->resize(QSize(700,700));
            ui->win_or_lose->setPixmap(QPixmap(":/image/resourcefile/hearthstone_win.png").scaled(700,700));
            ui->win_or_lose->show();
            timer.stop();
            mytimer.stop();
            timer_time.stop();
            enemy_move_time.stop();
            enemy_time.stop();
            pause_time++;
        }
    }
    if(bg_num==3&&stage<=3){
        if(b->hp!=0){
            bullet *eb1=new bullet(0,0);
            eb1->ph=13;
            bullet *eb2=new bullet(0,0);
            eb2->ph=13;
            bullet *eb3=new bullet(0,0);
            eb3->ph=13;
            eb1->setPixmap(QPixmap(":/image/resourcefile/ball-149921_1280.png").scaled(20,20));
            eb2->setPixmap(QPixmap(":/image/resourcefile/ball-149921_1280.png").scaled(20,20));
            eb3->setPixmap(QPixmap(":/image/resourcefile/ball-149921_1280.png").scaled(20,20));
            scene->addItem(eb1);
            scene->addItem(eb2);
            scene->addItem(eb3);
            int a=qrand()%2;
            if(a==0){
                eb1->setPos(m->pos().x()-m->pixmap().width(),m->pos().y()+m->pixmap().height());
                eb2->setPos(m->pos().x(),m->pos().y()+m->pixmap().height());
                eb3->setPos(m->pos().x()+m->pixmap().width(),m->pos().y()+m->pixmap().height());
                connect(&timer,SIGNAL(timeout()),eb1,SLOT(bfly1()));
                connect(&timer,SIGNAL(timeout()),eb2,SLOT(bfly1()));
                connect(&timer,SIGNAL(timeout()),eb3,SLOT(bfly1()));
            }
            if(a==1){
                eb1->setPos(m->pos().x()-m->pixmap().width(),m->pos().y()+m->pixmap().height());
                eb2->setPos(m->pos().x(),m->pos().y()+m->pixmap().height()+20);
                eb3->setPos(m->pos().x()+m->pixmap().width(),m->pos().y()+m->pixmap().height()+80);
                connect(&timer,SIGNAL(timeout()),eb1,SLOT(bfly2()));
                connect(&timer,SIGNAL(timeout()),eb2,SLOT(bfly2()));
                connect(&timer,SIGNAL(timeout()),eb3,SLOT(bfly4()));
            }
        }
        if(m->hp<=0){
            stage++;
            rest_time.start();
            timer.stop();
            mytimer.stop();
            timer_time.stop();
            enemy_move_time.stop();
            enemy_time.stop();
            if(stage<=max_stage){
                if(bg_num==3){
                    if(stage==2)m->setPixmap(QPixmap(":/image/resourcefile/tos_mob_2_1.png").scaled(50,50));
                    if(stage==3)m->setPixmap(QPixmap(":/image/resourcefile/tos_mob_7.png").scaled(50,50));
                    if(stage==4){
                        delete m;
                        b->setPos(375,50);
                        scene->addItem(b);
                        connect(&enemy_move_time,SIGNAL(timeout()),this,SLOT(enemy_move_boss()));
                        connect(&timer,SIGNAL(timeout()),b,SLOT(attack()));
                        connect(&timer,SIGNAL(timeout()),b,SLOT(attacked()));
                        b->setPixmap(QPixmap(":/image/resourcefile/8465326.png").scaled(100,100));
                        b->hp=90;
                    }
                    m->hp=30;
                }
            }
        }
    }

}
void MainWindow::time_pause(){
    if(pause_time%2==0){
        ui->pause->setIcon(QIcon(":/image/resourcefile/Play-icon-2.png"));
        timer.stop();
        mytimer.stop();
        timer_time.stop();
        enemy_move_time.stop();
        enemy_time.stop();
    }
    if(pause_time%2==1){
        ui->pause->setIcon(QIcon(":/image/resourcefile/Pause-icon.png"));
        timer.start();
        mytimer.start();
        timer_time.start();
        enemy_time.start();
        enemy_move_time.start();
    }
    pause_time++;
}
void MainWindow::take_a_rest(){
    count_rest++;
    timer.start();
    mytimer.start();
    timer_time.start();
    enemy_move_time.start();
    enemy_time.start();
    rest_time.stop();

}
void MainWindow::enemy_move_boss(){
    b->enemy_move(player->pos().x());
}
void MainWindow::skill_time_pass(){
    timer.start();
    enemy_move_time.start();
    enemy_time.start();
    skill_time.stop();
}
MainWindow::~MainWindow()
{
    delete ui;
}

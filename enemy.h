#ifndef ENEMY_H
#define ENEMY_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QLabel>
#include <time.h>
//#include "bullet.h"
class enemy : public QObject , public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit enemy(QObject *parent = nullptr);
    QList<QGraphicsItem*>list;
    int hp;
signals:

public slots:
    virtual void enemy_move();
    virtual void attack();
    virtual void attacked();
private:
    int num;
};

#endif // ENEMY_H

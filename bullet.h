#ifndef BULLET_H
#define BULLET_H
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QtMath>
#include "mainwindow.h"
class bullet : public QObject,public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit bullet(int,int,QObject *parent = nullptr);
    QList<QGraphicsItem*>list;
    int ph;
public slots:
    void fly1();
    void fly2();
    void fly3();
    void bfly1();
    void bfly2();
    void bfly3();
    void bfly4();
    void bfly5();
    void bfly6();
signals:

private:
    int v_x;
    int v_y;

};

#endif // BULLET_H

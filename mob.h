#ifndef MOB_H
#define MOB_H

#include "enemy.h"
class mob : public enemy
{
public:
    mob();
signals:

public slots:
    virtual void enemy_move();
    virtual void attack();
    virtual void attacked();
};

#endif // MOB_H
